#include "qwerty.hh"
#include <focus-signal-filter.hh>
#include <numpad.hh>

#include <ui_qwerty.h>

#include <QApplication>
#include <QDebug>
#include <QHash>
#include <QKeyEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QWindow>

QSize    Qwerty::size              = {570, 210};
QWidget *Qwerty::install_on        = nullptr;
Qwerty  *Qwerty::qwerty            = nullptr;
bool     Qwerty::is_close_on_enter = false;

void
Qwerty::install (QWidget &show_in, QWidget &install_on)
{
  // Install filter
  auto filter = new FocusSignalFilter (&install_on, &install_on);

  QObject::connect (
      filter, &FocusSignalFilter::sig_focus_in, [&show_in, &install_on] {
        if (Qwerty::install_on == &install_on) return;
        auto qwerty        = new Qwerty (&show_in);
        Qwerty::qwerty     = qwerty;
        Qwerty::install_on = &install_on;
        qwerty->set_geo ();
        if (is_close_on_enter)
          QMetaObject::invokeMethod (Qwerty::qwerty, "close_on_enter",
                                     Qt::DirectConnection);
        qwerty->show ();
      });
  QObject::connect (filter, &FocusSignalFilter::sig_focus_out, [&install_on] {
    if (Qwerty::qwerty) delete Qwerty::qwerty;
  });
}

/** @TODO: move this to filter so that each target can have different behavior.
 */
void
Qwerty::close_on_enter ()
{
  ui->btn_enter->disconnect ();
  QObject::connect (ui->btn_enter, &QPushButton::clicked, [this] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Enter, Qt::NoModifier, "\n");
    qApp->sendEvent (qApp->focusObject (), &ev);
    ui->btn_close->click ();
  });
}

void
Qwerty::set_enter_close ()
{
  is_close_on_enter = true;
}

Qwerty::Qwerty (QWidget *parent) : QWidget {parent}, ui (new Ui::Qwerty)
{
  ui->setupUi (this);

  // Init letter keys
  auto btnls = ui->letter_btn_grp->buttons ();
  for (auto i : btnls)
    { // Init letter keys
      const QString &key = i->objectName ().section ("_", -1, -1);
      QObject::connect (i, &QPushButton::clicked, [this, key] {
        QKeyEvent ev (
            QEvent::KeyPress,
            static_cast<Qt::Key> (key[0].digitValue () - 'a' + Qt::Key_A),
            is_caps ? Qt::ShiftModifier : Qt::NoModifier,
            is_caps ? key.toUpper () : key);
        qApp->sendEvent (qApp->focusObject (), &ev);
      });
    }

  // Init delete key
  QObject::connect (ui->btn_del, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Backspace, Qt::NoModifier, "\b");
    // qApp->sendEvent (qApp->focusObject (), &ev);
    qApp->sendEvent (qApp->focusObject (), &ev);
  });

  // Init Caps key
  QObject::connect (ui->btn_caps, &QPushButton::clicked,
                    [this] { toggle_caps (); });

  // Init Space key
  QObject::connect (ui->btn_space, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Space, Qt::NoModifier, " ");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });

  // Init numpad key
  QObject::connect (ui->btn_numpad, &QPushButton::clicked, [this] {
    if (numpad)
      {
        numpad->show ();
        return;
      }
    numpad = Numpad::show_once (*parentWidget ());
    numpad->use_hide_close ();
  });

  // Init comma key
  QObject::connect (ui->btn_comma, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Comma, Qt::NoModifier, ",");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });

  // Init dot key
  QObject::connect (ui->btn_dot, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Period, Qt::NoModifier, ".");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });

  // Init close btn
  QObject::connect (ui->btn_close, &QPushButton::clicked,
                    [] { qApp->focusWidget ()->clearFocus (); });
  // Init enter btn
  QObject::connect (ui->btn_enter, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Enter, Qt::NoModifier, "\n");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });
}

Qwerty::~Qwerty ()
{
  if (Qwerty::qwerty)
    {
      Qwerty::install_on = nullptr;
      Qwerty::qwerty     = nullptr;
    };
  delete numpad;
}

void
Qwerty::set_geo ()
{
  auto focus = qApp->focusWidget ();
  if (!focus || !qApp->focusWindow ()) return;

  // map coordination from parent of focus widget to parent of this
  QPoint top_left {};
  if (focus->parentWidget ())
    top_left = focus->parentWidget ()->mapTo (this->parentWidget (),
                                              focus->geometry ().topLeft ());
  QRect focus_geo;
  focus_geo.setTopLeft (top_left);
  focus_geo.setSize (focus->geometry ().size ());

  // We only use width and height of parent geometry.
  auto parent_geo = parentWidget ()->geometry ();
  if (parent_geo.height () == 0 || parent_geo.width () == 0)
    parent_geo = {0, 0, 1024, 600};

  QRect geo;
  if (focus_geo.y () + focus_geo.height () + size.height () + 5
      > parent_geo.height ()) // Show at boader
    geo.setY (parent_geo.height () - size.height () - 5);
  else geo.setY (focus_geo.y () + focus_geo.height () + 5);
  if (focus_geo.x () + size.width () > parent_geo.width ())
    // Show at boader
    geo.setX (parent_geo.width () - size.width () - 5);
  else geo.setX (focus_geo.x ());
  geo.setSize (size);
  setGeometry (geo);
}

// Qt 5.9 has no isUpper()
static bool
is_upper (const QString &s)
{
  for (const auto &i : s)
    if (!('A' <= i && i <= 'Z')) return false;
  return true;
}

void
Qwerty::toggle_caps ()
{
  auto btnls = ui->letter_btn_grp->buttons ();
  for (const auto i : btnls)
    {
      if (is_upper (i->text ())) i->setText (i->text ().toLower ());
      else i->setText (i->text ().toUpper ());
    }
  is_caps = !is_caps;
  if (is_caps) ui->btn_caps->setStyleSheet ("color: red;");
  else ui->btn_caps->setStyleSheet ("");
}

static inline bool
operator> (const QPoint &lhs, const QPoint &rhs)
{
  return lhs.x () > rhs.x () && lhs.y () > rhs.y ();
}

static inline bool
operator< (const QPoint &lhs, const QPoint &rhs)
{
  return lhs.x () < rhs.x () && lhs.y () < rhs.y ();
}

void
Qwerty::mousePressEvent (QMouseEvent *event)
{
  const auto &mv_area = ui->move_lbl->geometry (); // this coordination
  const auto &top_left
      = mapToParent (mv_area.topLeft ()); // this to parent coordination
  const auto &bottom_right
      = mapToParent (mv_area.bottomRight ()); // this to parent coordination
  const auto &ev_pos = mapToParent (event->pos ());
  if (ev_pos > top_left && ev_pos < bottom_right)
    {
      drag_offset = pos () - ev_pos;
      is_dragging = true;
    }
  event->accept ();
}

void
Qwerty::mouseReleaseEvent (QMouseEvent *event)
{
  is_dragging = false;
  event->accept ();
}

void
Qwerty::mouseMoveEvent (QMouseEvent *event)
{
  if (is_dragging) move (mapToParent (event->pos ()) + drag_offset);
  event->accept ();
}
