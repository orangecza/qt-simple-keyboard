#ifndef QWERTY_HH
#define QWERTY_HH

// TODO: Use a base class to refactor qwerty and numpad

#include <QWidget>

namespace Ui
{
class Qwerty;
};

class Numpad;
class Qwerty : public QWidget
{
  Q_OBJECT
public:
  static QSize size; // 760, 280
  static void  install (QWidget &show_in, QWidget &install_on);
  static void  set_enter_close ();

protected:
  void mousePressEvent (QMouseEvent *) override;
  void mouseReleaseEvent (QMouseEvent *) override;
  void mouseMoveEvent (QMouseEvent *) override;

private slots:
  void close_on_enter ();

private:
  Qwerty (QWidget *parent = nullptr);
  ~Qwerty ();
  void set_geo ();
  void toggle_caps ();

private:
  Ui::Qwerty *ui;

  QPoint  drag_offset;
  bool    is_dragging = false;
  bool    is_caps     = false;
  Numpad *numpad      = nullptr;

  static QWidget *install_on;
  static Qwerty  *qwerty;
  static bool     is_close_on_enter;
};

#endif
