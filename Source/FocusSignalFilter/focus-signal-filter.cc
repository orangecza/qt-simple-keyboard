#include "focus-signal-filter.hh"

#include <QEvent>
#include <QObject>

FocusSignalFilter::FocusSignalFilter (QObject *install_on, QObject *parent)
    : QObject {parent}, obj (install_on)
{
  install_on->installEventFilter (this);
}

bool
FocusSignalFilter::eventFilter (QObject *object, QEvent *event)
{
  if (object == obj)
    {
      if (event->type () == QEvent::FocusIn) emit sig_focus_in ();
      else if (event->type () == QEvent::FocusOut) emit sig_focus_out ();
    }
  return QObject::eventFilter (object, event);
}
