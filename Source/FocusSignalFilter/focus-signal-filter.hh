#ifndef FOCUS_SIGNAL_FILTER_HH
#define FOCUS_SIGNAL_FILTER_HH

#include <QObject>

class FocusSignalFilter : public QObject
{
  Q_OBJECT

public:
  FocusSignalFilter (QObject *install_on, QObject *parent = nullptr);
  bool eventFilter (QObject *object, QEvent *event) override;

signals:
  void sig_focus_in ();
  void sig_focus_out ();

private:
  QObject *obj;
};

#endif
