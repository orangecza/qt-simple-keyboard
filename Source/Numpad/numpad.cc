#include "numpad.hh"
#include <focus-signal-filter.hh>

#include <ui_numpad.h>

#include <QApplication>
#include <QDebug>
#include <QHash>
#include <QKeyEvent>
#include <QLabel>
#include <QMouseEvent>
#include <QWindow>

QSize    Numpad::size              = {195, 210};
QWidget *Numpad::install_on        = nullptr;
Numpad  *Numpad::numpad            = nullptr;
bool     Numpad::is_close_on_enter = false;
bool     Numpad::is_close_hide     = false;

void
Numpad::install (QWidget &show_in, QWidget &install_on)
{
  // Install filter
  auto filter = new FocusSignalFilter (&install_on, &install_on);

  QObject::connect (
      filter, &FocusSignalFilter::sig_focus_in, [&show_in, &install_on] {
        if (Numpad::install_on == &install_on) return;
        if (Numpad::numpad) delete Numpad::numpad;
        auto numpad = new Numpad (&show_in);
        Numpad::numpad     = numpad;
        Numpad::install_on = &install_on;
        Numpad::numpad->set_geo ();
        if (is_close_on_enter)
          QMetaObject::invokeMethod (Numpad::numpad, "close_on_enter",
                                     Qt::DirectConnection);
        if (is_close_hide)
          QMetaObject::invokeMethod (Numpad::numpad, "use_hide_close",
                                     Qt::DirectConnection);
        Numpad::numpad->QWidget::show ();
      });
  QObject::connect (filter, &FocusSignalFilter::sig_focus_out,
                    [&install_on] { delete Numpad::numpad; });
}

void
Numpad::use_hide_close ()
{
  ui->btn_close->disconnect ();
  QObject::connect (ui->btn_close, &QPushButton::clicked, [this] { hide (); });
}

Numpad *
Numpad::show_once (QWidget &show_in)
{
  auto numpad = new Numpad (&show_in);
  numpad->set_geo ();
  numpad->show ();
  return numpad;
}

void
Numpad::close_on_enter ()
{
  ui->btn_enter->disconnect ();
  QObject::connect (ui->btn_enter, &QPushButton::clicked, [this] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Enter, Qt::NoModifier, "\n");
    qApp->sendEvent (qApp->focusObject (), &ev);
    ui->btn_close->click ();
  });
}

void
Numpad::set_close_hide ()
{
  is_close_hide = true;
}
void
Numpad::set_enter_close ()
{
  is_close_on_enter = true;
}

Numpad::Numpad (QWidget *parent) : QWidget {parent}, ui (new Ui::Numpad)
{
  ui->setupUi (this);

  // Init keys
  auto    btnls = ui->num_btn_grp->buttons ();
  QString key;
  for (auto i : btnls)
    {
      key = i->objectName ().section ("_", -1, -1);
      QObject::connect (i, &QPushButton::clicked, [key] {
        QKeyEvent ev (QEvent::KeyPress,
                      static_cast<Qt::Key> (key.toInt () + Qt::Key_0),
                      Qt::NoModifier, key);
        qApp->sendEvent (qApp->focusObject (), &ev);
      });
    }
  QObject::connect (ui->btn_del, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Backspace, Qt::NoModifier, "\b");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });

  QObject::connect (ui->btn_enter, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Enter, Qt::NoModifier, "\n");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });
  QObject::connect (ui->btn_dot, &QPushButton::clicked, [] {
    QKeyEvent ev (QEvent::KeyPress, Qt::Key_Period, Qt::NoModifier, ".");
    qApp->sendEvent (qApp->focusObject (), &ev);
  });
  QObject::connect (ui->btn_close, &QPushButton::clicked,
                    [] { qApp->focusWidget ()->clearFocus (); });
  // QObject::connect (qApp, &QApplication::focusChanged,
  //                   [this] { delete this; });
}

Numpad::~Numpad ()
{
  if (Numpad::numpad)
    {
      Numpad::numpad     = nullptr;
      Numpad::install_on = nullptr;
    }
}

// Should be inherit from a parent class, actually.
// *we just use it for now xD*
void
Numpad::set_geo ()
{
  auto focus = qApp->focusWidget ();
  if (!focus || !qApp->focusWindow ()) return;

  // map coordination from parent of focus widget to parent of this
  QPoint top_left {};
  if (focus->parentWidget ())
    top_left = focus->parentWidget ()->mapTo (this->parentWidget (),
                                              focus->geometry ().topLeft ());
  QRect focus_geo;
  focus_geo.setTopLeft (top_left);
  focus_geo.setSize (focus->geometry ().size ());

  // We only use width and height of parent geometry.
  auto parent_geo = parentWidget ()->geometry ();
  if (parent_geo.height () == 0 || parent_geo.width () == 0)
    parent_geo = {0, 0, 1024, 600};

  QRect geo;
  if (focus_geo.y () + focus_geo.height () + size.height () + 5
      > parent_geo.height ()) // Show at boader
    geo.setY (parent_geo.height () - size.height () - 5);
  else geo.setY (focus_geo.y () + focus_geo.height () + 5);
  if (focus_geo.x () + size.width () > parent_geo.width ())
    // Show at boader
    geo.setX (parent_geo.width () - size.width () - 5);
  else geo.setX (focus_geo.x ());
  geo.setSize (size);
  setGeometry (geo);
}

static inline bool
operator> (const QPoint &lhs, const QPoint &rhs)
{
  return lhs.x () > rhs.x () && lhs.y () > rhs.y ();
}

static inline bool
operator< (const QPoint &lhs, const QPoint &rhs)
{
  return lhs.x () < rhs.x () && lhs.y () < rhs.y ();
}

void
Numpad::mousePressEvent (QMouseEvent *event)
{
  const auto &mv_area = ui->move_lbl->geometry (); // this coordination
  const auto &top_left
      = mapToParent (mv_area.topLeft ()); // this to parent coordination
  const auto &bottom_right
      = mapToParent (mv_area.bottomRight ()); // this to parent coordination
  const auto &ev_pos = mapToParent (event->pos ());
  if (ev_pos > top_left && ev_pos < bottom_right)
    {
      drag_offset = pos () - ev_pos;
      is_dragging = true;
    }
  event->accept ();
}

void
Numpad::mouseReleaseEvent (QMouseEvent *event)
{
  is_dragging = false;
  event->accept ();
}

void
Numpad::mouseMoveEvent (QMouseEvent *event)
{
  if (is_dragging) move (mapToParent (event->pos ()) + drag_offset);
  event->accept ();
}
