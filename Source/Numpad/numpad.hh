#ifndef NUMPAD_HH
#define NUMPAD_HH

#include <QWidget>

namespace Ui
{
class Numpad;
};

class Numpad : public QWidget
{
  Q_OBJECT
public:
  static QSize size; // 210, 280
  static void  install (QWidget &show_in, QWidget &install_on);
  /** @note: @show_in will be set as parent of new numpad.  */
  static Numpad *show_once (QWidget &show_in);

  static void set_close_hide ();
  static void set_enter_close ();

  ~Numpad ();

public slots:
  void use_hide_close ();
  void close_on_enter ();

protected:
  void mousePressEvent (QMouseEvent *) override;
  void mouseReleaseEvent (QMouseEvent *) override;
  void mouseMoveEvent (QMouseEvent *) override;

private:
  Numpad (QWidget *parent = nullptr);
  void set_geo ();

private:
  static Numpad *numpad;
  Ui::Numpad *ui;

  QPoint drag_offset;
  bool   is_dragging = false;

  static bool is_close_on_enter;
  static bool is_close_hide;

  static QWidget *install_on;
};

#endif
