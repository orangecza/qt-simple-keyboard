#include <numpad.hh>
#include <qwerty.hh>

#include <QApplication>
#include <QDebug>
#include <QLineEdit>
#include <QTimer>
#include <QVBoxLayout>

int
main (int argc, char *argv[])
{
  QApplication app (argc, argv);
  QWidget      w;

  w.setGeometry ({0, 0, 1024, 768});
  w.setLayout (new QVBoxLayout);

  QLineEdit *e1 = new QLineEdit (&w);
  QLineEdit *e2 = new QLineEdit (&w);
  w.layout ()->addWidget (e1);
  w.layout ()->addWidget (e2);

  Numpad::install (w, *e1);
  Qwerty::install (w, *e2);
  Qwerty::set_enter_close();

  w.show ();
  app.exec ();
}
