TEMPLATE = lib #app
QT       += core gui widgets
CONFIG += c++14 static

SRC_DIR = Source
LIB_DIR = Library
RES_DIR = Resource
MOD_DIR = Reference

# Objects to be build
OBJS = Numpad FocusSignalFilter Qwerty #Test
# Modules that this module reference
MODS =
# 3rd libraries
LIBRARYS =

# --- --- --- --- --- --- ---

include (module.pri)
