SUBPRO_PATH = $${_PRO_FILE_PWD_}
for (OBJ, OBJS): INCLUDEPATH += $${SUBPRO_PATH}/$${SRC_DIR}/$${OBJ} # -I$${OBJ}
for (LIB, LIBRARYS): INCLUDEPATH += $${SUBPRO_PATH}/$${LIB_DIR}/$${LIB}
INCLUDEPATH += $${SUBPRO_PATH}/$${MOD_DIR}
for (MOD, MODS): INCLUDEPATH += $${SUBPRO_PATH}/$${MOD_DIR}/$${MOD}
for (LIB, LIBRARYS): LIBS += -L$${SUBPRO_PATH}/$${LIB_DIR}/$${LIB}
isEmpty (LIBRARYS){} else {LIBS += -Wl,--start-group}
for (LIB, LIBRARYS): LIBS += -l$${LIB}
isEmpty (LIBRARYS){} else {LIBS += -Wl,--end-group}
for (LIB, LIBRARYS): LIBS += -l$${LIB}

QMAKE_CXXFLAGS += -g
QMAKE_LFLAGS   += -Wl,-rpath=./lib

for (OBJ, OBJS): SOURCES    += $$files($${SUBPRO_PATH}/$${SRC_DIR}/$${OBJ}/*.cc, true)
for (OBJ, OBJS): HEADERS    += $$files($${SUBPRO_PATH}/$${SRC_DIR}/$${OBJ}/*.hh, true)
for (OBJ, OBJS): FORMS      += $$files($${SUBPRO_PATH}/$${SRC_DIR}/$${OBJ}/*.ui, true)
RESOURCES  += $$files($${SUBPRO_PATH}/$${RES_DIR}/*.qrc, true)

DESTDIR     = .build
OBJECTS_DIR = $${DESTDIR}/obj
MOC_DIR     = $${DESTDIR}/moc
RCC_DIR     = $${DESTDIR}/rcc
UI_DIR      = $${DESTDIR}/uic

# Search and compile modules
contains(CONFIG, static) {}
else {
    for (MOD, MODS) {
        build_$${MOD}.target = $${SUBPRO_PATH}/../$${MOD}/.build/lib$${MOD}.a
        build_$${MOD}.depends = makefile_$${MOD}
        build_$${MOD}.commands = make -C $${SUBPRO_PATH}/../$${MOD}
        build_$${MOD}.CONFIG = phony
        makefile_$${MOD}.target = $${SUBPRO_PATH}/../$${MOD}/Makefile
        makefile_$${MOD}.commands = qmake $${SUBPRO_PATH}/../$${MOD}/$${MOD}.pro -o $${SUBPRO_PATH}/../$${MOD}/Makefile
        QMAKE_EXTRA_TARGETS += makefile_$${MOD} build_$${MOD}
        PRE_TARGETDEPS += $${SUBPRO_PATH}/../$${MOD}/.build/lib$${MOD}.a
    }
    for (MOD, MODS) : LIBS += -L$${SUBPRO_PATH}/../$${MOD}/.build
    isEmpty (MODS){} else {LIBS += -Wl,--start-group}
    for (MOD, MODS) : LIBS += -l$${MOD}
    isEmpty (MODS){} else {LIBS += -Wl,--end-group}
}

IDE_BUILD_DIR =
INCLUDEPATH += $${SUBPRO_PATH}/$${IDE_BUILD_DIR}/$${UI_DIR}
